# TDD

- Cloner ce repo
- Rechercher et expliquer avec vos mots les principes du tdd, et l'ajouter a ce Readme.
- Observer fizzbuzz.test.js puis completer fizzbuzz.js pour qu'il passe les tests ("npm run test" pour lancer les tests)

- Creer un fichier calc.js dans /src contenant une fonction calc()
- Creer un fichier calc.test.js dans /test
- Ecrire les test d'une fonction calc qui permet d'additionner/soustraire/multiplier/diviser 2 nombres envoyé a la fonction calc, 
avec gestion de toutes les erreurs possibles, en utilisant la méthode tdd.
- commit entre chaque ecriture de test et chaque ecriture de fonction

Définition du TDD : Le TDD ou Test-Driven Development est le fait de réduire les problèmes des application en mettant en place une série fréquente de test. Il permet de corrigé les erreurs rapidement avant de lancer la production. 