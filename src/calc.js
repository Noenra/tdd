const calc = (num1, calcul, num2) => {
    if(typeof num1 != "number" || typeof num2 != "number" || calcul != "+" && calcul != "-" && calcul != "*" && calcul != "/"){
        return "Error!"
    }
    if(calcul == "+"){
        if(parseInt(num1) && parseInt(num2)){
            return num1 + num2
        }
        if(num1.toString().length == num2.toString().length){
            return parseFloat((num1 + num2).toFixed((num1.toString().length)-2))
        }
        if(num1.toString().length < num2.toString().length || num1.toString().length > num2.toString().length){
            return parseFloat((num1+num2).toFixed(((num1 + num2).toString().length)-2))
        }
    }
    if(calcul == "-"){
        if(parseInt(num1) && parseInt(num2)){
            return num1 - num2
        }
        if(num1.toString().length == num2.toString().length){
            return parseFloat((num1 - num2).toFixed((num1.toString().length)-2))
        }
        if(num1.toString().length < num2.toString().length || num1.toString().length > num2.toString().length){
            return parseFloat((num1-num2).toFixed(((num1 - num2).toString().length)-2))
        }
    }

}

exports.calc = calc;