const fizzbuzz = (num) => {
    if(num == null){
        return 'Error!'
    }
    if(num % 5 == 0 && num % 7 ==0){
        return "fizzbuzz"
    }
    if(num % 5 == 0){
        return "buzz"
    }
    if(num % 7 == 0){
        return "fizz"
    }
    if(num % 5 != 0 || num % 7 != 0){
        return ""
    }
}

exports.fizzbuzz = fizzbuzz;