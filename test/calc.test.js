const expect = require('chai').expect;
const {calc} = require('../src/calc');

describe("calc", () => {
    it("Should return 8 if it's an addition.", () => {
        expect(calc(6, "+", 2)).to.equal(8);
    });
    it("Should return 0.8 if it's an addition.", () => {
        expect(calc(0.1, "+", 0.7)).to.equal(0.8);
    });
    it("Should return 0.008 if it's an addition.", () => {
        expect(calc(0.001, "+", 0.007)).to.equal(0.008);
    });
    it("Should return 0.100000000007 if it's an addition.", () => {
        expect(calc(0.1, "+", 0.000000000007)).to.equal(0.100000000007);
    });
    it("Should return 235.100000000000000007 if it's an addition.", () => {
        expect(calc(234.000000000000000007, "+", 1.1)).to.equal(235.100000000000000007);
    });

    
    it("Should return 4 if it's an addition.", () => {
        expect(calc(6, "-", 2)).to.equal(4);
    });
    it("Should return 0.8 if it's an addition.", () => {
        expect(calc(0.7, "-", 0.1)).to.equal(0.6);
    });
    it("Should return 0.008 if it's an addition.", () => {
        expect(calc(0.007, "-", 0.001)).to.equal(0.006);
    });
    it("Should return 0.09999999999 if it's an addition.", () => {
        expect(calc(0.1, "-", 0.000000000007)).to.equal(0.09999999999300001);
    });
    it("Should return 232.9 if it's an addition.", () => {
        expect(calc(234.000000000000000007, "-", 1.1)).to.equal(232.9);
    });


    it("handles 1 answers", () => {
        expect(calc(6)).to.contain('Error!');
    });
    it("handles empty answers", () => {
        expect(calc()).to.contain('Error!');
    })
    it("handles 1 string other than a +, -, *, /", () => {
        expect(calc(6, "A", 2)).to.contain('Error!');
    });
    it("handles 1 string other than a number", () => {
        expect(calc("", "+", 2)).to.contain('Error!');
    });
})